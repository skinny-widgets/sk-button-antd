
import { SkButtonImpl }  from '../../sk-button/src/impl/sk-button-impl.js';

export class AntdSkButton extends SkButtonImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'button';
    }

    afterRendered() {
        super.afterRendered();
        if (this.buttonType) {
            this.button.classList.add(`ant-btn-${this.buttonType}`);
        }
        this.mountStyles();
    }
}
